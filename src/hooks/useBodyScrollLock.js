import { useLayoutEffect } from 'react';

function useBodyScrollLock() {
  useLayoutEffect(() => {
    //const originalOverflow = window.getComputedStyle(document.body).overflow;
    //console.log(originalOverflow);

    document.body.style.overflow = 'hidden';

    return () => {
      document.body.style.overflow = '';
    };
  }, []);
}

export { useBodyScrollLock };
